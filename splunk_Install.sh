#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run with sudo privileges"
   exit 1
fi

echo "###########################################################"
echo "#     What this script does:                              #"
echo "#         +Download the latest Splunk Enterprise version  #"
echo "#         +Install Splunk Enterprise                      #"
echo "#         +Create Splunk account and lock it              #"
echo "#         +Setup Splunk environmental variables           #"
echo "#         +Configure Splunk web to use HTTPS              #"
echo "#         +Configure Splunk to startup as Splunk          #"
echo "#         +Configure FirewallD with standard ports        #"
echo "###########################################################"

URL="https://www.splunk.com/en_us/download/splunk-enterprise.html"
OS_REGEX="linux-2\.6-x86_64\.rpm"
CONTENT=`curl -s --connect-timeout 10 --max-time 10 $URL`
LINK=`echo $CONTENT | egrep -o "data-link=\"https://[^\"]+-${OS_REGEX}\"" | cut -c12- | rev | cut -c2- | rev`

if [[ "$LINK" == "https://"* ]]; then
   echo "Download link: $LINK"
   echo ""
   wget --no-check-certificate -P /tmp $LINK -O splunk_enterprise_install.rpm && echo "############### Splunk Enterprise Download Successful ###################"
else
   echo "Error: Could not get download link"
   exit 1
fi

echo "############### Creating Splunk User and Group ###################"
groupadd splunk
useradd -g splunk -d /opt/splunk splunk
cp /etc/skel/.* /opt/splunk/
passwd -l splunk

echo "############### Installing Splunk ###################"
(rpm -i splunk_enterprise_install.rpm && echo "############### Splunk Install Complete ###################") || echo "############### Installing Splunk ###################"

############ Configure Splunk ############
echo "############### Starting Splunk ###################"
/opt/splunk/bin/splunk start --accept-license --answer-yes 

echo "############### Stoping Splunk ###################"
/opt/splunk/bin/splunk stop

echo "############### Configuring Splunk Bootstrap and Environmental Variables ###################"
/opt/splunk/bin/splunk enable boot-start -user splunk
chown -R splunk:splunk /opt/splunk
su - splunk -c '/opt/splunk/bin/splunk start'
su - splunk -c 'echo '\''export PATH=$PATH:/opt/splunk/bin'\'' >>~/.bash_profile'
su - splunk -c 'echo '\''export SPLUNK_HOME=/opt/splunk'\'' >>~/.bash_profile'
su - splunk -c 'source /opt/splunk/bin/setSplunkEnv'

echo "############### Configuring Splunk to run HTTPS ###################"
su - splunk -c 'echo [settings] >> ~/etc/system/local/web.conf'
su - splunk -c 'echo "enableSplunkWebSSL = true" >> ~/etc/system/local/web.conf'

############ Configure FirewallD ############
echo "############### Configuring FirewallD ###################"
firewall-cmd --zone=public --add-port=8000/tcp --permanent
firewall-cmd --zone=public --add-port=8089/tcp --permanent
firewall-cmd --zone=public --add-port=9997/tcp --permanent
firewall-cmd --reload
firewall-cmd --list-all

echo "############### Starting Splunk ###################"
/opt/splunk/bin/splunk start